package model.data_structures.test;

import java.util.Iterator;
import java.util.Random;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase {
	
	private ListaDobleEncadenada<String> lista;
	
	/*
	 * Con un elemento
	 */
	public void setupEscenario1(){
		lista = new ListaDobleEncadenada<String>();	 
		lista.agregarElementoFinal("0");
	}
	
	/*
	 * Con 10 elemento
	 */
	public void setupEscenario2(){
		lista = new ListaDobleEncadenada<String>();	
		for(int i=0; i<10; i++){
			lista.agregarElementoFinal(i +" ");
		}
	}
	
	/*
	 * Con n�mero aleatorio de elementos (No cero)
	 */
	public void setupEscenario3(){
		lista = new ListaDobleEncadenada<String>();
		Random r = new Random();
		for(int i=0; i<(Math.abs(r.nextInt()))+1; i++){
			lista.agregarElementoFinal(i +" ");
		}
	}
	
	
	public void testAddLast(){
		setupEscenario1();
		assertEquals("La lista deberia tener un elemento", 1, lista.darNumeroElementos());
		assertEquals("La lista deberia tener solamente un 0" , "0" , lista.darElemento(0));
		assertEquals("Los nodos primero, actual y �ltimo deber�an ser los mismos" , true , lista.ultimo == lista.primero &&  lista.actual == lista.primero);
		
		setupEscenario2();
		lista.agregarElementoFinal("11 ");
		assertEquals("El tamanio de la lista debe ser 11", 11, lista.darNumeroElementos());
		assertEquals("El elemento en la posici�n 10 debe existir y ser igual "
				+ "a 11", "11 " , lista.darElemento(10));
		
	}
	
	public void testGet(){
		setupEscenario1();
		assertEquals("deber�a retornar el elemento '0'", "0 ".trim(), lista.darElemento(0).trim());
		assertTrue("No deber�a haber elementos en la posici�n 1 ",null==lista.darElemento(1));
		
		setupEscenario2();
		assertEquals("deber�a retornar el elemento '0'", "0 ", lista.darElemento(0));
		assertFalse("Deber�a haber elementos en la posici�n 9", null==lista.darElemento(9));
		
		setupEscenario3();
		assertFalse("Deber�a existir el elemento de la posici�n 0", null==lista.darElemento(0));
		assertFalse("Deber�a existir este elemento", null==lista.darElemento(lista.darNumeroElementos()-1));
		
		
	}
	
	public void testSize(){
		setupEscenario1();
		assertEquals("Deber�a tener tamanio 1", 1, lista.darNumeroElementos());
		
		setupEscenario2();
		assertEquals("Deber�a tener tamanio 10", 10, lista.darNumeroElementos());
		
		setupEscenario3();
		assertTrue(lista.darNumeroElementos()>0);
		
		
	}
	
	public void testCurrent(){
		setupEscenario1();
		assertEquals("El acutal deber�a ser igual al primero y al �ltimo",lista.primero, lista.actual);
		assertEquals("El acutal deber�a ser igual al primero y al �ltimo",lista.ultimo, lista.actual);
	
		setupEscenario2();
		assertEquals("El acutal deber�a ser igual al primero",lista.primero, lista.actual);
		for(int i=0; i<5;i++){
			lista.avanzarSiguientePosicion();
		}
		assertEquals("El acutal deber�a ser igual al de la posici�n 5 ",lista.darElemento(5), lista.actual.elemento);
		
		setupEscenario3();
		assertEquals("El acutal deber�a ser igual al primero",lista.primero, lista.actual);
		for(int i=0; i<(lista.darNumeroElementos()/2) +1;i++){
			lista.avanzarSiguientePosicion();
		}
		//System.out.println(lista.darNumeroElementos());
		//System.out.println(lista.darElemento((lista.darNumeroElementos()/2)) );
		//System.out.println(lista.actual.elemento);
		
		assertEquals("El acutal deber�a ser igual al de la posici�n dada ",lista.darElemento((lista.darNumeroElementos()/2) +1), lista.actual.elemento);
		
	
	}
	
	public void testFwd(){
	
		setupEscenario1();
		assertFalse(lista.avanzarSiguientePosicion());
		
		setupEscenario2();
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("El elemento actual ya deber�a ser el de la posici�n 1",lista.darElemento(1), lista.actual.elemento );
		
	}
	
	public void testBackwd(){
		setupEscenario1();
		assertFalse(lista.retrocederPosicionAnterior());
		
		setupEscenario2();
		System.out.println(lista.actual);
		assertTrue(lista.avanzarSiguientePosicion());
		System.out.println(lista.actual);
		assertTrue(lista.avanzarSiguientePosicion());
		System.out.println(lista.actual);
		assertTrue(lista.avanzarSiguientePosicion());
		System.out.println(lista.actual);
		assertTrue(lista.retrocederPosicionAnterior());
		System.out.println(lista.actual);
		assertTrue(lista.retrocederPosicionAnterior());
		System.out.println(lista.actual);
		assertEquals("El elemento actual ya deber�a ser el de la posici�n 1",lista.darElemento(1), lista.actual.elemento );
		

		assertEquals("El elemento actual ya deber�a ser el de la posici�n 2",lista.darElemento(2), lista.actual.elemento );
	
	}
	
	
		
	public void testIterator(){
		setupEscenario2();
		Iterator<String> iter = lista.iterator();
		int i = 0; 
		while(iter.hasNext()){
			String sup = iter.next();
//			System.out.println("sup: "+sup);
			String real = lista.darElemento(i);
//			System.out.println("real: "+real);
			assertEquals("El  numero deberi ser " + real+"pero fue " + sup,real, sup);
			i++;
		}
	}
	

}

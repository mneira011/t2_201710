package model.data_structures.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase {
	
	private ListaEncadenada<String> lista; 
	
	public void setupEscenario1(){
		lista = new ListaEncadenada<String>();
		lista.agregarElementoFinal("4");
		
		
	}
	public void setupEscenario2(){
		lista = new ListaEncadenada<String>();
//		System.out.println("Agregados:");
		for(int i = 0  ; i<10 ; i++){
			lista.agregarElementoFinal(i + "");
//			System.out.println(lista.darElemento(i));
			
		}
//		System.out.println("");
	}
	public int setupEscenario3(){
		lista = new ListaEncadenada<String>();
		
		Random rGenerator = new Random();
		int num = rGenerator.nextInt(1000);
		for(int i = 0 ; i < num ; i++){
			lista.agregarElementoFinal(i+"");
			
		}
		return num;
		
	}
	
	
	
	public void testAgregar(){
		setupEscenario1();
//		lista.print();
		assertEquals("La lista deberia tener un elemento", 1, lista.darNumeroElementos());
		assertEquals("La lista deberia tener solamente un 4" , "4" , lista.darElemento(0));
		assertEquals("La lista debera tener la cabeza apuntando hacia el 4","4",lista.darElementoPosicionActual());
	}
	public void testDarElemento(){
		setupEscenario2();
//		lista.print();
		for(int i = 0 ; i<10 ; i++){
			assertEquals("La lista deberiea tener " + i + "en la posicion " + i , i+"", lista.darElemento(i) );
		}
	}
	
	public void testIterator(){
		setupEscenario2();
		Iterator<String> iter = lista.iterator();
		int i = 0; 
		while(iter.hasNext()){
			String sup = iter.next();
//			System.out.println("sup: "+sup);
			String real = lista.darElemento(i);
//			System.out.println("real: "+real);
			assertEquals("El  numero deberi ser " + real+"pero fue " + sup,real, sup);
			i++;
		}
	}
	public void testNumeroElementos(){
		int num=setupEscenario3();
		int aProbar = lista.darNumeroElementos();
		
		assertEquals("El numero de elementos que deberia tener es " + num + "pero se obtuvo " + aProbar , num, aProbar);
		
	}
	public void testDarElementoPorPosicion(){
		int size = setupEscenario3();
		assertEquals("Actual deberia estar en el primer elemento", lista.darElemento(0),lista.darElementoPosicionActual());
		int i = size/2 +2; 
		String numActual = lista.darElemento(i);
		String actual = lista.darElementoPosicionActual();
		assertEquals("Actual deberia tener "+ numActual +"pero tenia " + actual, numActual,actual);
		
	}
	
	public void testAvanzarSiguientePosicion(){
		int size = setupEscenario3();
		int i = size/2 ; 
		String numActual = lista.darElemento(i);
		lista.avanzarSiguientePosicion();
		String actual = lista.darElementoPosicionActual();
		numActual = lista.darElemento(i+1);
		assertEquals("Actual deberia tener "+ numActual +"pero tenia " + actual, numActual,actual);
		
	}
	public void testRetrocederPosicionAnterior(){
		int size = setupEscenario3();
		int i = size-1; 
		String numActual = lista.darElemento(i);
		lista.retrocederPosicionAnterior();
		String actual = lista.darElementoPosicionActual();
		numActual = lista.darElemento(i-1);
		assertEquals("Actual deberia tener "+ numActual +"pero tenia " + actual, numActual,actual);
		
	}
	
	

}

package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.swing.plaf.synth.SynthSeparatorUI;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub



		//		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		misPeliculas = new ListaEncadenada<VOPelicula>();
		int ianio = 1950;
		int fanio = 2016;
		int anios = fanio -ianio +1; 
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

		for(int i = 0 ; i <anios ; i++){
			VOAgnoPelicula aAgregarPeliculasAgno = new VOAgnoPelicula();
			aAgregarPeliculasAgno.setAgno(i+ianio);
			aAgregarPeliculasAgno.setPeliculas(new ListaEncadenada<VOPelicula>());
			peliculasAgno.agregarElementoFinal(aAgregarPeliculasAgno);
			//			peliculasAgno.agregarElementoFinal(aAgregar);
		}

		try(BufferedReader bf = new BufferedReader(new FileReader(archivoPeliculas));){
			String str;
			//primera linea es "movieId,title,genres"
			str = bf.readLine();
			int cont = 1; 
			while((str = bf.readLine())!=null){

				//TODOOO ESTO ES PARA TENER LA INFORMACION LINDA

				String[] linea = str.split(",");
				String[] linea3 = new String[3];
				int tamanio = linea.length;

				if(linea.length>3){
					String sanduche = "";

					for(int j = 0 ; j<tamanio-2;j++){
						if(j!=tamanio-3)
							sanduche += linea[j+1]+",";
						else
							sanduche += linea[j+1];
					}
					//					System.out.println(sanduche);
					linea3[0] = linea[0];
					linea3[1] = sanduche;

					linea3[2] = linea[3];

				}else{
					linea3 = linea;
				}


				String movieID = linea3[0];
				String[] name = linea3[1].replaceAll(Pattern.quote("\""),"").split("[(]");
				String titulo = name[0];
				String[] anioS;
				int anio;
				//				System.out.println(nombre);
				if(name.length==2){
					anioS = (name[1].replaceAll(Pattern.quote(")"),"").trim().split("-"));
					anio = Integer.parseInt(anioS[0]);
//					System.out.println(anio);
				}
				else{
					anio = 0;
				}



				String[] genres = linea3[2].split("|");
				ListaDobleEncadenada<String> generos = new ListaDobleEncadenada<>();
				for(String genero : genres){
					generos.agregarElementoFinal(genero);
				}
				//				System.out.println(anio);

				//Agregamos pelicula a misPeliculas
				
				

				VOPelicula aAgregar = new VOPelicula();
				aAgregar.setTitulo(titulo);
				aAgregar.setAgnoPublicacion(anio);
				aAgregar.setGenerosAsociados(generos);
				misPeliculas.agregarElementoFinal(aAgregar);

				//				le vamos a hacer pistola a las peliculas que no tienen año
				if(anio<=2016&&anio>=1950){
//					if(anio<1950&&anio>2016){
//						System.out.println(anio + " " + cont);
//					}
					peliculasAgno.darElemento(anio-1950).getPeliculas().agregarElementoFinal(aAgregar);
//					System.out.println(peliculasAgno.darElemento(anio-1950).getPeliculas().darElemento(0).getTitulo());

				}

				cont ++;

			}



		}
		catch(IOException e){
			e.printStackTrace();
		}

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPelicula> res = new ListaEncadenada<>();
		
		for(VOPelicula peli : misPeliculas){
			if(peli.getTitulo().contains(busqueda)){
				res.agregarElementoFinal(peli);
			}
		}
		return res;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		// TODO Auto-generated method stub
		if(agno<1950||agno>2016){
			System.out.println("Por favor buscar peliculas dentro del rango 1950-2916");

		}
		else{
			return peliculasAgno.darElemento(agno - 1950).getPeliculas();
		}

		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}

}

package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	public NodoDoble<T> primero;
	public NodoDoble<T> actual;
	public NodoDoble<T> ultimo;
	private int tamanio;


	public ListaDobleEncadenada(){
		ultimo = primero;
		actual = primero;
	}

	public class NodoDoble<T>{
		public T elemento;
		public NodoDoble<T> siguiente;
		public NodoDoble<T> anterior;

		public NodoDoble(T elemento){
			this.elemento = elemento;
		}
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new iterador();
	}
	protected class iterador implements Iterator<T>   {
		NodoDoble<T> nodo;
		public iterador(){
			nodo = primero; 
		}
		public boolean hasNext() {
			return nodo.siguiente!=null;

		}
		public T next() {
			NodoDoble<T> n = nodo;
			nodo = nodo.siguiente;
			return n.elemento;

		}
		public void remove(){
			throw new UnsupportedOperationException("No soportado.");
		}
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(primero==null){
			primero = new NodoDoble<T>(elem);
			ultimo = primero;
			actual = primero;
		}
		else{
			NodoDoble<T> temp =ultimo;
			
			ultimo.siguiente = new NodoDoble<T>(elem);
			ultimo = ultimo.siguiente;
			ultimo.anterior =temp;
		}
		tamanio++;

	}

	@Override
	public T darElemento(int pos) {
		NodoDoble<T> n = primero;
		for(;pos > 0 && n != null; pos--)
			n = n.siguiente;
		actual = n==null ? actual : n;
		return n == null ? null : n.elemento;
	}


	@Override
	public int darNumeroElementos() {
		return tamanio;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.elemento;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if (actual== null || actual.siguiente ==null)
			return false;
		else{
			actual = actual.siguiente;
			return true;
		}



	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if (actual== null || actual.anterior ==null){

			System.out.println("False");
			return false;
		}
		else{
			actual = actual.anterior;
			return true;
		}
	}


}

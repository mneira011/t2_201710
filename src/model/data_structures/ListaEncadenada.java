package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {
	
	NodoSencillo<T> cabeza;
	NodoSencillo<T> actual;
	NodoSencillo<T> cola;
	int size; 
	
	public ListaEncadenada() {
		// TODO Auto-generated constructor stub
		size = 0; 
		cabeza = null;
		actual = null;
		cola = null;
		
		
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterador  it = new Iterador();
		return it;
		
		
	}
	
	public void print(){
	
		if(darNumeroElementos()!=0){
			for(int i = 0 ; i<darNumeroElementos() ; i++){
				System.out.println(darElemento(i));
			}
		}
		else{
			System.out.println("La lista esta vacia");
		}
	}
	
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> aAgregar = new NodoSencillo<>(elem);
		if(size ==0){
			
			cabeza = actual = cola = aAgregar;
			
		}
		else{
			cola.cambiarSiguiente(aAgregar);
			cola = aAgregar;
			
		}
		size++;
		
		
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		T ans = null;
		if(pos>=0 && pos<size){
			NodoSencillo<T> aIterar = cabeza;
			if(pos!=0){
				for(int i = 0 ; i < pos ; i++){
					aIterar = aIterar.darSiguiente();
					
				}
			}
			actual = aIterar;
			ans = aIterar.darInfo();
		}
		return ans;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darInfo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		boolean ans = false; 
		if(cabeza !=null && actual.darSiguiente()!=null){
			
			actual = actual.darSiguiente();
			ans =true;
		}
		return ans;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean ans = false; 
		if(cabeza!=null&&actual!=cabeza){
			ans = true;
			NodoSencillo<T> nods = null; 
			Iterator<T> it = iterator();
			while(it.hasNext()){
				T recorrer = (T) it.next(); 
				if (nods == null){
					nods = cabeza;
				}
				else{
					nods = nods.darSiguiente();
							
				}
				if(nods.darSiguiente()==actual){
					actual = nods;
					break;
				}
			}
		}
		return ans;
		
		
	}
	
	
	private class NodoSencillo<A>{
		private NodoSencillo<A> siguiente;
		
		private A info; 
		
		public NodoSencillo(A pInfo){
			info = pInfo;
			siguiente = null; 
		}
		
		public NodoSencillo(A pInfo, NodoSencillo<A> pSiguiente){
			info = pInfo;
			siguiente = pSiguiente; 
		}
		
		public void cambiarInfo(A pInfo){
			info = pInfo;
			
		}
		public boolean tieneSiguiente(){
			boolean ans = false;
			if(siguiente!=null){
				ans = true;
			}
			return ans; 
		}

		public NodoSencillo<A> darSiguiente(){
			return siguiente; 
			
		}
		public void cambiarSiguiente(NodoSencillo<A> pSiguiente){
			siguiente = pSiguiente;
		}
		public A darInfo(){
			return info; 
		}
		
		
	}

	protected class Iterador implements Iterator<T>   {
        protected NodoSencillo<T> pos;
        public Iterador(){
        	 pos = null; 
        }
        public boolean hasNext() {
        	if(pos==null&&size!=0)
        		return true;
        	else if(pos==null){
        		return false;
        	}
        	else{
        		return pos.tieneSiguiente();
        	}
            
        }
        public T next() {
        	 
        	if(pos==null){
        		pos = cabeza; 
        		return pos.darInfo();
        	}
        	else{
        		
        		pos = pos.darSiguiente();
        		T ans = pos.darInfo();
        		return ans;
        	}
        }
        public void remove(){
            throw new UnsupportedOperationException("No soportado.");
        }
    }
	

 

}


